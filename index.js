var express = require('express');
var app = express();
var request = require('request');
var jsforce = require('jsforce');

var authorization = 'GET FROM PRECISELY LOCAL STORAGE';

var client_id = 'SALESFORCE SPECIFIC';
var client_secret = 'SALESFORCE SPECIFIC';

// set the port of our application
// process.env.PORT lets the port be set by Heroku
var port = process.env.PORT || 8080;

// set the view engine to ejs
app.set('view engine', 'ejs');

// make express look in the public directory for assets (css/js/img)
app.use(express.static(__dirname + '/public'));

// set the home page route
app.get('/', function(req, res) {

    // ejs render automatically looks in the views folder
    res.render('index');
});

// set the callback route
app.get('/callback', function(req, res) {
    console.log(req.query);
    if(req.query.code) {
        request({
            url: `https://login.salesforce.com/services/oauth2/token?grant_type=authorization_code&client_secret=${client_secret}&client_id=${client_id}&redirect_uri=https%3A%2F%2Fpeaceful-sierra-62797.herokuapp.com%2Fcallback&code=${req.query.code}`,
            method: 'POST',
            json: true,
            body: ''
        }, function (error, response, body) {
            console.log(response);
            console.log(response.body);
            var conn = new jsforce.Connection({
                oauth2: {
                    clientId: client_id,
                    clientSecret: client_secret,
                    redirectUri: 'https://peaceful-sierra-62797.herokuapp.com/callback'
                },
                instanceUrl: response.body.instance_url,
                accessToken: response.body.access_token,
                refreshToken: response.body.refresh_token
            });
            conn.identity(function(err, result) {
                if (err) { return console.error(err); }
                console.log("user ID: " + result.user_id);
                console.log("will set Precisely token to " + authorization);
                conn.sobject("User").update({ 
                    Id : result.user_id,
                    PreciselyToken__c : authorization,
                  }, function(err, ret) {
                    if (err || !ret.success) { return console.error(err, ret); }
                    console.log('Updated Successfully : ' + ret.id);
                    console.log('res : ' + result);
                    res.render('callback');
                  });
              });
        }
        );
    }
});



app.listen(port, function() {
    console.log('Our app is running on port ' + port);
});